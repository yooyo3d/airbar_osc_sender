﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using extOSC;
using extOSC.Serialization;
using SimpleJSON;

public class OSCTouchSender : MonoBehaviour {

    public class OSCTouch
    {
        [OSCSerialize]
        public int index;
        [OSCSerialize]
        public int maxIndex;
        [OSCSerialize]
        public float altitudeAngle;
        [OSCSerialize]
        public float azimuthAngle;
        [OSCSerialize]
        public Vector2 deltaPosition;
        [OSCSerialize]
        public float deltaTime;
        [OSCSerialize]
        public int fingerId;
        [OSCSerialize]
        public float maximumPossiblePressure;
        [OSCSerialize]
        public TouchPhase phase;
        [OSCSerialize]
        public Vector2 position;
        [OSCSerialize]
        public float pressure;
        [OSCSerialize]
        public float radius;
        [OSCSerialize]
        public float radiusVariance;
        [OSCSerialize]
        public Vector2 rawPosition;
        [OSCSerialize]
        public int tapCount;
        [OSCSerialize]
        public TouchType type;
        public OSCTouch() { }
        public OSCTouch(Touch t, int idx, int maxidx)
        {
            index = idx;
            maxIndex = maxidx;
            altitudeAngle = t.altitudeAngle;
            azimuthAngle = t.azimuthAngle;
            deltaPosition = t.deltaPosition;
            deltaTime = t.deltaTime;
            fingerId = t.fingerId;
            maximumPossiblePressure = t.maximumPossiblePressure;
            phase = t.phase;
            position = t.position;
            pressure = t.pressure;
            radius = t.radius;
            radiusVariance = t.radiusVariance;
            rawPosition = t.rawPosition;
            tapCount = t.tapCount;
            type = t.type;
        }
    }
    #region Public Vars

    public string Address1 = "/AirBar/touch";
    public string Address2 = "/AirBar/count";

    [Header("OSC Settings")]
    public OSCTransmitter Transmitter;

    #endregion

    #region Unity Methods

    public string RemoteAddress="127.0.0.1";
    public int RemotePort=7000;
    protected void LoadConfig()
    {
        try
        {
            System.IO.StreamReader file = new System.IO.StreamReader("./config.json");
            string jsonFile = file.ReadToEnd();
            file.Close();
            JSONNode node = JSON.Parse(jsonFile);
            if (node.Tag == JSONNodeType.Object)
            {
                RemoteAddress = node["address"];
                RemotePort = node["port"].AsInt;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public void Awake()
    {
        LoadConfig();
        Transmitter.RemoteHost = RemoteAddress;
        Transmitter.RemotePort = RemotePort;
    }
    protected virtual void Start()
    {
        Transmitter.Connect();
//         var message = new OSCMessage(Address);
//         message.AddValue(OSCValue.String("Hello, world!"));
// 
//         Debug.Log("sending");
//         Transmitter.Send(message);
    }

    #endregion

    // Update is called once per frame
    void Update ()
    {
        var message2 = new OSCMessage(Address2);
        message2.AddValue(OSCValue.Int(Input.touchCount));
        Transmitter.Send(message2);

        for (int i=0; i<Input.touchCount; i++)
        {
            OSCTouch t = new OSCTouch(Input.touches[i], i, Input.touchCount);
            var message = OSCSerializer.Serialize(Address1, t);
            Transmitter.Send(message);
        }
    }
};
